This project contains "handover" issues for Engineering Productivity's internal rotation.

* [Handover issues](https://gitlab.com/gitlab-org/quality/engineering-productivity/internal-rotation/-/issues)
* [Internal Rotation schedule](https://gitlab.com/gitlab-org/quality/engineering-productivity/master-broken-incidents/-/oncall_schedules)

## Why not "on-call"?

Technically (and financially) it's not a real on-call so we call it "Internal Rotation".