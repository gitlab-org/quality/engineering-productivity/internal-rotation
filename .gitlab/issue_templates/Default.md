<!--
   Title: Handover - Week YYYY-MM-DD

   YYYY-MM-DD is the first day of the week.
-->

## Participants

Pick one!

<!--

* @kkloss
* @nao.hashizume

/assign @kkloss @nao.hashizume

OR

* @ddieulivol
* @jennli 

/assign @ddieulivol @jennli

-->

## Weekly checklist

Complete the tasks below **at least once** for the week.

### Dashboards

**Important:** Run the dashboards (top-right button) before analysing the data. The dashboards don't auto-update!

* [ ] Look at [EP - Pipeline durations](https://app.snowflake.com/ys68254/gitlab/#/ep-pipeline-durations-d4NWA2TAT)
  dashboard and see if there's anything unusual. Pay more attention to weekly
  because this is a weekly rotation.
* [ ] Look at [EP - Job durations](https://app.snowflake.com/ys68254/gitlab/#/ep-job-durations-dPkG7M61u)
  dashboard and see if there's anything unusual.
* [ ] Look at [EP - Pipeline success rates](https://app.snowflake.com/ys68254/gitlab/#/ep-pipeline-success-rates-d4GqRCjeZ)
  dashboard and see if there's anything unusual. Ideally success rates and
  not retried rates should be both very high.

### Other

* [ ] [Go through ~"flakiness::1" tests that were not quarantined](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/pipelines.md#go-through-flakiness1-tests-that-were-not-quarantined)
* [ ] [Go through the weekly EP Triage Report](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/meta.md#go-through-the-weekly-ep-triage-report)
* [ ] Go through [triage-ops errors](https://new-sentry.gitlab.net/organizations/gitlab/issues/?project=20&query=&referrer=project-issue-stream), and create issues accordingly
  * [ ] Please add the ~"Sentry" label to this new issue.
* [ ] Triage the [feature flag issues](https://gitlab.com/gitlab-org/quality/triage-reports/-/issues/?sort=created_date&state=opened&label_name%5B%5D=feature%20flag&or%5Bassignee_username%5D%5B%5D=ddieulivol&or%5Bassignee_username%5D%5B%5D=jennli&or%5Bassignee_username%5D%5B%5D=nao.hashizume&or%5Bassignee_username%5D%5B%5D=rymai&or%5Bassignee_username%5D%5B%5D=godfat-gitlab&or%5Bassignee_username%5D%5B%5D=splattael&or%5Bassignee_username%5D%5B%5D=kkloss&first_page_size=100) (triaging instructions are in the issue)
* [ ] [Review the slowest tests/test files in rspec_profiling_stats](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/pipelines.md#review-the-slowest-teststest-files-in-rspec_profiling_stats)

## Monthly checklist

Complete the tasks below **once during the first part (i.e. first 15 days) of each month**. If applicable, check that [it wasn't done last week](https://gitlab.com/gitlab-org/quality/engineering-productivity/internal-rotation/-/issues/?sort=created_date&state=opened&first_page_size=100).

### Access tokens

* [ ] Check for [access tokens about to expire in the next 30 days](https://internal.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/rotating-credentials/#check-for-tokens-that-are-about-to-expire)

Complete the tasks below **once during the second part (i.e. last 15 days) of each month**. If applicable, check that [it wasn't done last week](https://gitlab.com/gitlab-org/quality/engineering-productivity/internal-rotation/-/issues/?sort=created_date&state=opened&first_page_size=100).

### Review-apps

* [ ] [Review review-apps cluster upgrades](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/review-apps.md#manually-upgrade-the-gke-cluster)
* [ ] [Upgrade review-apps helm chart](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/review-apps.md#upgrade-gitlab-helm-chart)
* [ ] [Check the GCP console cluster page](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/review-apps.md#check-the-gcp-console-cluster-page)
* [ ] [Upgrade kubectl in EP container images](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/review-apps.md#upgrade-kubectl-in-ep-container-images)

## Workflow

Create a thread per day with the following:

```markdown
## YYYY-MM-DD
```

Then, write a comment below this thread:

```markdown
## EMEA/AMER

### Planned work

* [ ] Cleanup open [master broken incidents](https://gitlab.com/gitlab-org/quality/engineering-productivity/master-broken-incidents/-/issues)
  * Important: We only have to **handle the incidents that don't have a group label assigned to them** (i.e. it's for EP to handle by default). We can surely assist/nudge the other groups to fix the master-broken incidents assigned to them.
  * Monitor [#master-broken](https://gitlab.enterprise.slack.com/archives/CR6QH3D7C)
* [ ] Cleanup open [approved MR pipeline incidents](https://gitlab.com/gitlab-org/quality/engineering-productivity/approved-mr-pipeline-incidents/-/issues)
  * Monitor [#mr-blocked-by-master-broken](https://gitlab.enterprise.slack.com/archives/C06KFK5EE73)
* [ ] Help and unblock people at [#g_engineering_productivity](https://gitlab.enterprise.slack.com/archives/CMA7DQJRX)

### Noteworthy

* xxx

### Other reactive work

* xxx
```

## Refs

* [Internal rotation schedule](https://gitlab.com/gitlab-org/quality/engineering-productivity/master-broken-incidents/-/oncall_schedules)
